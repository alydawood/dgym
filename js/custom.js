$(document).ready(function(){

 if( $('body').hasClass('path-gym-classes-view')
     && $('div').hasClass('views-row')
     && $('div').hasClass('card')
     ){
        $('div.view-content').addClass('row');
        $('div.views-row').addClass('col col-sm-6');
        $('.card-img-top img').removeAttr('width');
        $('.card-img-top img').removeAttr('height');
        $('.card-img-top img').addClass('img-fluid');
        $('.page-title').hide();




    }
    if(window.location.href.indexOf("/gallery/") >0 ){
        $('.field__items').addClass('row');
        $('.field__item').addClass('col col-xs-12 col-sm-6 col-md-4');
        $('.field__item img').addClass('img-fluid');
        $('#block-bootstrap4-page-title > h1 > span').addClass('single-page-title');

    }
    if(window.location.href.indexOf("/gym-classes/") >0 ){
        console.log("hello");
        $('#block-bootstrap4-page-title > h1 > span').addClass('single-page-title');
        let selector1="article > div > div.field.field--name-field-image.field--type-image.field--label-hidden.field__item > img";

        $(selector1).parent().css("text-align","center");
        $(selector1).removeAttr('width');
        $(selector1).removeAttr('height');
        $(selector1).addClass('single-page-img-fluid');

        let selector2="article > div > div.field.field--name-field-day-of-week.field--type-list-string.field--label-hidden.field__item";
        let selector3="article > div > div.field.field--name-field-class-start-time.field--type-time-range-picker.field--label-hidden.field__item";


        $(selector2).css("display","inline-block");
        $(selector2).addClass("single-page-class-day");

        $(selector3).css("display","inline-block");
        $(selector3).addClass("single-page-class-day");



    }// if single page for gym-classes

});
